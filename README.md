# pyNATO

## Description
Take a word or phrase and return it as NATO alphabet characters.

## Usage

Run with `python3 pynato.py`

Type word or phrase you would like to have translated and press 'Enter'

Exit from loop with CTRL-C

## Requires
Python3
