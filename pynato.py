#!/bin/python3

def get_nato(words, mode='standard'):

    if mode.lower() == 'standard':
        nato_dict = {'A': 'Alfa', 'B': 'Bravo', 'C': 'Charlie', 'D': 'Delta', 'E': 'Echo', 'F': 'Foxtrot', 'G': 'Golf', 'H': 'Hotel', 'I': 'India', 'J': 'Juliett', 'K': 'Kilo', 'L': 'Lima', 'M': 'Mike', 'N': 'November', 'O': 'Oscar', 'P': 'Papa', 'Q': 'Quebec', 'R': 'Romeo', 'S': 'Sierra', 'T': 'Tango', 'U': 'Uniform', 'V': 'Victor', 'W': 'Whiskey', 'X': 'Xray', 'Y': 'Yankee', 'Z': 'Zulu', '0': 'Zero', '1': 'One', '2': 'Two', '3': 'Three', '4': 'Four', '5': 'Five', '6': 'Six', '7': 'Seven', '8': 'Eight', '9': 'Nine'}
    elif mode.lower() == 'archer':
        nato_dict = {'A': 'Alfa', 'B': 'Bravo', 'C': 'Charlie', 'D': 'Delta', 'E': 'Echo', 'F': 'Foxtrot', 'G': 'Golf', 'H': 'Hotel', 'I': 'India', 'J': 'Juliett', 'K': 'Kilo', 'L': 'Lima', 'M': 'Mancy', 'N': 'November', 'O': 'Oscar', 'P': 'Papa', 'Q': 'Quebec', 'R': 'Romeo', 'S': 'Sierra', 'T': 'Tango', 'U': 'Uniform', 'V': 'Victor', 'W': 'Whiskey', 'X': 'Xray', 'Y': 'Yankee', 'Z': 'Zulu', '0': 'Zero', '1': 'One', '2': 'Two', '3': 'Three', '4': 'Four', '5': 'Five', '6': 'Six', '7': 'Seven', '8': 'Eight', '9': 'Nine'}
    elif mode.lower() == 'usa':
        nato_dict = {'A': 'Apple Pie', 'B': 'Butterfinger', 'C': 'Chocolate', 'D': 'Donuts', 'E': 'Eclair', 'F': 'French Fries', 'G': 'Gummy Bears', 'H': 'Ho-Hos', 'I': 'Ice Cream', 'J': 'Jelly Beans', 'K': 'Kit-Kats', 'L': 'Licorice', 'M': "M&M's", 'N': 'Nutella', 'O': 'Oreo', 'P': 'Pie', 'Q': 'Queso', 'R': "Reese's Pieces", 'S': 'Skittles', 'T': 'Twinkies', 'U': 'Upside-down cake', 'V': 'Vanilla pudding', 'W': 'Whipped cream', 'X': 'Xtra Cheddar Goldfish', 'Y': 'York Peppermint', 'Z': 'Zebra Cakes'}
    elif mode.lower() == 'oreo':
        pass
    elif mode.lower() == 'antinato':
        pass

    letters = list(words)
    for letter in letters:
        if letter.isalnum():
            print(nato_dict[letter.upper()], end=' ')
        else:
            print(letter, end=' ')

def main():
    from sys import argv

    try:
        mode = argv[1]
    except:
        mode = 'standard'

    while True:
        words = input('Please input word or phrase: ')
        print()
        get_nato(words, mode=mode)
        print('\n\n')

if __name__ == '__main__':
    main()
